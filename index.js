// Phillip Baumman, Conor Nailos, Justin Peek, Michael Wang
// Week of April 24, 2017
// Data Structures Final - Denali Descent: Choose Your Own Adventure

var Alexa = require('alexa-sdk');

var states = {
    STARTMODE: '_STARTMODE',                // Prompt the user to start or restart the game.
    STORYMODE: '_STORYMODE',                // Alexa is telling the user the story
};


// Nodes/Events of the story
var nodes = [{ "node": 1, "message": "You wake up confused and cold. As you look around you, you behold a stunning view of the surrounding mountain range. Memories rush back as you remember your team’s precarious summit and failed rescue. You quickly stand, feeling light-headed you scan the snowbank for your teammates. Seeing a backpack rising from the snow, you quickly rush to your fallen teammate. Todd, whose skin on his face, at least what is showing, has gone blue. You shake him and find his body has gone stiff. Would you like to take a moment to assess the situation?", "yes": 2, "no": 3 },
             { "node": 2, "message": "You find a couple other team members and realize they have undergone the same fate as Todd. You take a moment to let out a few tears and understand what happened. You would cry more, but the tears freeze on your face. You search your teammates’ supplies and only take what you need - some extra food rations, an ice pick, extra batteries, and another first aid kit. You put this in your bag, along with your own food, a compass, a first aid kit, a knife, and a single space tent. You decide to move on. Which face of the mountain will you descend? Traverse the steep westward snowbank from which you came, or continue on the rocky eastward path stretching forwards?", "west": 4, "east": 5 },
             { "node": 3, "message": "You find a couple other team members and realize they have undergone the same fate as Todd.  However, you know you must move quickly or else you’ll join them.  You search their supplies and take what you need - some extra food rations, extra batteries, and an extra first aid kit. You put this in your bag, along with your own food, a compass, a first aid kit, a knife, and a single space tent. You decide to move on. Which face of the mountain will you descend? Traverse the steep westward snowbank from which you came, or continue on the rocky eastward path stretching forwards?", "west": 4, "east": 5 },
             { "node": 4, "message": "You start your descent, shimmying along the steep bank you make it several hundred feet down before reaching a patch of ice. As you attempt to catch the surrounding snow with your pick, your pick slices air as you quickly slide. Picking up speed, you see a crevasse approaching. As you slip over the edge, you know this is the end. A thud resonates through the crevasse as your body hits the ground. The mountain takes another as it quickly resumes its silent vigil. You are dead. Would you like to play again?", "yes": 1, "no": 0 },
             { "node": 5, "message": "You start your descent down the rocky path. The path is not too steep, and the rocks give you a relatively stable path. You make decent progress down the mountain without too much issue, but you still have a long ways to go. The sun is going down and you notice the air is getting even colder and you’re losing a lot of light. Do you set up the tent or try to find a cave to sleep in for the night?", "cave": 	6, "tent": 7 },
             { "node": 6, "message": "You approach a nearby cave and are immediately confronted with an overwhelming stench emanating from the cave. Would you still like to enter?", "yes": 12, "no": 13 },
             { "node": 7, "message": "As the sun sets on the mountain, an aching chill spreads throughout your body. You pull the single-person tent out of your pack and begin setting it up on a dry patch sheltered from the wind. Before you climb into your tent, is there anything you forgot to do? Spending time here could impact your recovery.", "yes": 8, "no": 9 },
             { "node": 8, "message": "Should you explore your area or store your food away from your tent?", "explore": 10, "store": 11 },
             { "node": 9, "message": "To aid in your recovery process, you climb into your tent. Reflecting on the day reduces you to tears as the loneliness of your situation sits in. As you drift into sleep from pure exhaustion, you can’t help but feel like you forgot something very important. You wake up at dawn after a dreamless night with horrible hunger pangs and an achy body. As you turn to look at your bag, you find it ravaged. Your food supplies are gone, but you must continue on. You throw the backpack on and push on. As you start your walk, you see two types of berry bushes - a red, smooth berry and a red, wrinkled berry. Do you eat the smooth berries or the wrinkled berries?", "smooth": 16, "wrinkled": 17 },
             { "node": 10, "message": "Before going to bed, you explore your surroundings. Unfortunately, you don’t find much other than a few giant prints leading to and from a nearby cave. Would you like to follow the footsteps or return to sleep?", "follow": 6, "sleep": 9 },
             { "node": 11, "message": "Calling back on all you’ve learned in the last few days from your guide, you collect your food, toothpaste, and anything else that could attract animals and tie it up high in a tree. Knowing you’ll be safe tonight, you return to your tent and slip into a deep sleep. You wake up at dawn feeling reinvigorated. You will yourself up and retrieve your hanging belongings. You eat a couple energy bars and a pack of dried fruit before taking down your tent and packing everything. You continue on your trek down the mountain while the sun is coming above the surrounding treeline below you, feeling warmer than usual. As you sit to catch your breath on a nearby rock, you hear a noise, growing louder by the second. You look up the mountain and see a massive flood of snow coming down the mountain. Do you run to the side of the mountain, run down the mountain, or grab onto the rock you are sitting on?", "side": 19, "down": 20 , "rock": 21},
             { "node": 12, "message": "As you get closer and closer to the cave, you can’t help but gag at the smell. You cover your nose with a bandana and continue on. Crouching low, you attempt to sneak into the cave. A few feet into the darkness, a terrifying roar reverberates throughout the cave. Flying backwards, searing pain spreads throughout your chest. Two beady yellow eyes stare at you as you struggle to crawl towards the light. A giant white claw clutches your backpack as you attempt to shuffle out. Slowly this beast raises you in the air, turning you to face it as you realize what you’ve found. A yeti’s jaws close around your head. You can’t help but feel some relief as you no longer need to continue on your journey. You are dead. Would you like to try again?", "yes": 1, "no": 0 , },
             { "node": 13, "message": "You quickly rush back into your tent and go to sleep, fearful of the tracks you found. You wake up at dawn after a dreamless night with horrible hunger pangs and an achy body. As you turn to look at your bag, you find it ravaged. Your food supplies are gone, but you must continue on. You throw the backpack on and push on. As you start your walk, you see two types of berry bushes - a red, smooth berry and a red, wrinkled berry. Do you eat the smooth berries or the wrinkled berries?", "smooth": 16, "wrinkled": 17 , },
             { "node": 16, "message": "Something tells you that the smooth berries are safer and you precariously but hungrily eat the berries. They taste tart but have a bit of sweetness to them, almost like raspberries. You grab some more to take along the way. Suddenly you feel some faint rumbling underneath your feet. You think that you must be imagining things but the shaking becomes more violent. Up on the mountain you can see movement of snow and it dawns upon you that an avalanche is coming. Thinking quickly, do you try to run to the side of the mountain, run down the mountain, or hide behind a rock?", "side": 19, "down": 20, "rock": 21 },
             { "node": 17, "message": "You hungrily pull the wrinkled berries off the bushes. You immediately feel invigorated, thankful you remembered your guide’s instructions. Always eat the wrinkled berries, or was it never eat the wrinkled berries? Immediately you begin vomiting what little food remained in your stomach. The taste of stomach bile remains in your mouth as your consciousness slowly fades. You wake up some time later to what sounds like a stampede of bulls tearing down the mountain. You look up at the peak to see a wave of snow crashing down. In your weakened state the snow hits you like a train, knocking what little will to live you had left. You are dead.", "yes": 14, "no": 123 },
             { "node": 18, "message": "You hear what sounds like a stampede of bulls tearing down the mountain. You look up the mountain and see a massive flood of snow coming down the mountain. Do you run to the side of the mountain, run down the mountain, or grab onto the rock you are sitting on?", "side": 19, "down": 20, "rock": 21 },
             { "node": 19, "message": "You rush to the side, hoping to dodge the wall of snow quickly approaching. You dive ahead, evading the avalanche which rushes by. You stand, shaken up, but prepared for the rest of the day. You continue on. A long day of hiking has left you exhausted. As sunset rapidly approaches you decide to rest for the night. You want to set up camp however you see a shadowy figure near the trees next to the camp are. Do you set up camp where you are right now or avoid the shadowy figure and go on a little further?", "camp": 23, "go": 24},
             { "node": 20, "message": "You turn around and begin sprinting down the mountain. Your attempts to outrun the avalanche ultimately fail as the wall of snow envelops you and your pack. You're knocked unconscious by the brute force of the mountain hitting you from behind. You suffocate under several tons of snow. You are dead. Would you like to try again?", "yes": 18, "no": 123 },
             { "node": 21, "message": "You cling to the rock you sat on with all your might. Snow pounds against you, attempting to drag you down the mountain with several tons of snow. As you hang on for dear life, your mind flashes memories over the past few days. As you feel your strength begin to dwindle, the avalanche passes and you are fine. You stand, beaten but alive, you continue on your way. A long day of hiking has left you exhausted. As sunset rapidly approaches you decide to rest for the night. You want to set up camp however you see a shadowy figure near the trees next to the camp are. Do you camp where you are right now or avoid the shadowy figure and go on a little further.", "camp": 23, "go": 24 },
             { "node": 23, "message": "You decide that you must be seeing things and decide to set up camp where you are. You wake up next day and realize it was just a coat, hanging from a tree which probably got there from the avalanche. Do you search the coat or no?", "yes": 25, "no": 26 },
             { "node": 24, "message": "You press forward despite the rapid approach of night. You find an outcropping to camp in and decide that this would be best for tonight. The next day you come back to the forest and realize that the shadowy figure was just a coat. You think that it's better not to touch other people’s things and continue along your way. A little ways on you come across a crevasse. You remember your guide had mentioned this point in the descent. It stretches in both directions and spans a few feet wide. Would you like to jump the crevasse or attempt to walk around?", "jump": 27, "walk": 41 }, 
             { "node": 25, "message": "You search the coat, finding a flashlight and a compass. Thinking these will be useful for a future situation, you throw them in your pack and continue on your way. A little way on in the day, you come across a crevasse. It stretches in both directions and spans a few feet wide. Would you like to jump the crevasse or attempt to walk around?", "jump": 28, "walk": 30 },
             { "node": 27, "message": "You attempt to jump the crevasse. Starting several feet back, you begin to run, quickly reaching a sprint you jump with everything you have… You hit the lip and rebound back. You go weightless as you fall down the crevasse. You stare up at the sky  as the mountain envelops you. You hit the ground and are knocked unconscious. You wake up in pure darkness. As you roll to stand, a piercing pain stretches across your ribs. You realize that without a light you will not survive. You spend the next few hours wandering the caves till you fall. You can feel your legs break as you tumble through the cave. You die a few days later from your injuries. Would you like to try again?", "yes": 23, "no": 123 },
             { "node": 28, "message": "You attempt to jump the crevasse. Starting several feet back, you begin to run, quickly reaching a sprint you jump with everything you have… You hit the lip and rebound back. You go weightless as you fall down the crevasse. You stare up at the sky  as the mountain envelops you. You hit the ground and are knocked unconscious. You wake up in pure darkness. You think that the best way to get out is to take more risks and to adventure forward. You begin to explore the cave with your flashlight and compass. You know that the nearest town is eastward and so you try to make decisions based on that.  You reach a fork, one path points east and looks like it will bring you deeper underground. The other path looks like it goes above ground and points west, however, there are dried up bones littered on the ground. Do you go east or west?", "east": 31, "west": 32 },
             { "node": 29, "message": "Rather than turn back, you decide to wait near the bear cub. A mother bear soon arrives and eats your face off. You are dead. Would you like to try again?", "yes": 28, "no": 123 },
             { "node": 30, "message": "You decide not to jump and walk around, you walk for a while, do you wanna jump?", "yes": 42, "no": 41 },
             { "node": 31, "message": "You follow the path to the east. Using the compass. You hear water to the east and nothing to the west. Do you continue east or take the path west?", "east": 33, "west": 34 },
             { "node": 32, "message": "You follow the path to the west. You stumble upon a small baby cub sleeping on the ground. Do you walk around it or wake it up and try to keep it as a pet.", "walk": 35, "pet": 36 },
             { "node": 33, "message": "Following the path east, you find a running river with water that looks safe to drink.  You feel thirsty however you are speculative as to whether you should trust the water. Do you want to drink the water?", "yes": 37, "no": 38 },
             { "node": 34, "message": "Thinking to yourself that you should avoid the water, you go west with your flashlight and compass through the cave. As you continue on your way through the cave, you stumble over a jutting rock. You trip and tumble down the slope. You hit your head and die. Would you like to try again?", "yes": 28, "no": 123}, 
             { "node": 35, "message": "You decide to walk around the bear cub because obviously you don’t want to disturb it in fear of its parents. You keep walking but you realize that this is a dead end. Would you like to wait near the cub or retrace your steps?", "wait": 29, "retrace": 28 },
             { "node": 36, "message": "You decide that a pet bear cub would be the coolest thing ever. You gently rub its head and the furry little critter wakes up and yawns. Upon seeing you with your bright flashlight, it panics and runs away. You cry “no” as you only wanted to be friends and chase after it. Suddenly you hear a roar and a big brown bear comes hurtling towards you and kills you. You are dead. Would you like to try again?", "yes": 28, "no": 123},
             { "node": 37, "message": "You drink the water and immediately feel your thirst go away. Continuing alongside the river, you spot a small opening in the top of the cave. Do you try to climb up to the opening on the slippery rocks or try to find some other way?", "climb": 39, "other": 40 },
             { "node": 38, "message": "You decide not to drink the water however, you feel really parched and become nauseous. In a daze you wander around the cave and collapse. You wake up and realize you are back where you started.  You begin to explore the cave with your flashlight and compass. You know that the nearest town is eastward and so you try to make decisions based on that.  You reach a fork, one path points east and looks like it will bring you deeper underground. The other path looks like it goes above ground and points west, however, there are dried up bones littered on the ground. Do you go east or west?", "east": 31, "west": 32},
             { "node": 39, "message": "You excitedly begin climbing up the rocks and making your way to the small opening with light. You suddenly lose your footing and hit your head on the ground below, you die. Would you like to try again?", "yes": 28, "no": 123},
             { "node": 40, "message": "Taking things precautiously, you continue along the cave in hopes for some better path to get through the opening. Suddenly you spot a ladderlike structure along the cave wall. You begin to climb up the ladder and push on the ceiling on the cave. You push the earth out of the way to reveal a small wooden latch. You open the latch and climb up to see yourself standing in the middle of a cabin. Congrats. You have made it back to civilization! You descended Denali."},
             { "node": 41, "message": "Rather than jump a crevasse, you decide to walk around. After walking for what feels like hours, a wind begins to pick up. As you walk, you begin reconsidering your options. Would you like to jump or walk?", "jump": 42, "walk": 43 },
             { "node": 42, "message": "With this storm and what feels like a never ending crevasse, you decide to jump. As you line up to jump, you realize the wind is really beginning to pick up. Snow flurries batter you from every side. Are you sure you should jump?", "yes": 48, "no": 43},
             { "node": 43, "message": "As you continue walking, the wind begins pushing you back. You struggle on, catching what appears to be the end of the crevasse a short ways ahead through the heavy snowfall.  The wind and snow is really picking up now. The end of the crevasse is in sight. Should you advance or bunker down?", "advance": 44, "bunker": 49 },
             { "node": 44, "message": "You trudge through the rough weather. You make it to the edge of the crevasse and shuffle around the mouth. The wind catches your backpack and drags you towards the edge. Right before you slip off the edge, you swing your ice pick out into the nearby bank and stop yourself from falling. You don’t think you can keep going in this weather. You see an alcove ahead, do you stop or advance?", "advance": 45, "stop": 46 },
             { "node": 45, "message": "As you approach the alcove, you see a small bundle of brown in the alcove. Something in your gut tells you to run, but that could just be because you're hungry. The weather is pretty horrible too. Do you enter the alcove or advance?", "advance": 46, "alcove": 47 },
             { "node": 46, "message": "Something feels off about the alcove, you decide not to test your luck. As you continue on, you see giant paw prints headed towards the alcove. You think you made the right choice as the storm pushes against you. You walk for an hour or so. You energy is almost completely depleted as the sun sets and the wind dies down. You set down for the night. Do you want to eat before sleeping?", "yes": 50, "no": 51 },
             { "node": 47, "message": "As you get closer, you see the bundle move. What you thought was a brown bundle was actually a bear cub. Right when you realize this, a thunderous roar erupts from behind you. You flashback to your guides warnings, never get between a grizzly and her cub. You were mauled by the mother grizzly bear. You are dead. Would you like to try again?", "yes": 45, "no": 123 },
             { "node": 48, "message": "The wind batters your body as you line up perpendicular with the crevasse. You begin running, right before you jump, a gust of wind pushes your movement forwards. As you fly through the air, you swing your ice pick forwards attempting to catch the ice wall in front of you. Your pick slices air as you fall to your death. You are dead. Would you like to try again?", "yes": 42, "no": 123 },
             { "node": 49, "message": "Rather than push through the snow, you attempt to bunker down and conserve your energy. Chills begin to spread through your body as you attempt to conserve your body heat. It seems to start working as your body starts getting hot. You slowly feel drowsier and drowsier as you see your fingers begin to turn blue. You peacefully slip into death. The mountain claims another victim. You are dead. Would you like to try again?", "yes": 43, "no": 123 },
             { "node": 50, "message": "You scarf down what remains of your provisions. Your consciousness begins to fade as you drift off to sleep. You are awakened the next morning by the sound of something rummaging around outside. Peeking outside, you see that a pack of wolves is wandering around your campsite. Their attention turns to you as your head comes into view. A wolf notices you and stalks toward you. You’re worried about what will happen if the whole pack notices you. Do you run, play dead, or attempt to fight the wolf.", "run": 52, "playdead": 53, "fightthewolf": 54 },
             { "node": 51, "message": "You quickly pass out, deciding to reserve what little food you have left, you fall asleep. your eyes fly open as you hear a noise near your tent, as you peek your head out, you see a pack of wolves opening your food and examining camp, they approach the opening of your tent, they are scary, do you run, play dead, or fight the wolf", "run": 52, "playdead": 53, "fightthewolf": 54 },
             { "node": 52, "message": "Terrified of what will happen if you stay put you attempt to get out of your tent and flee. The pack immediately takes chase and tears you apart. You have died. Would you like to try again?", "yes": 50, "no": 123 },
             { "node": 53, "message": "You try to play dead as the wolves sniff around your tent. They begin tearing away at your tent and notice your body inside. As they begin to attack you realize this is the end. You have died. Would you like to try again?", "yes": 50, "no": 123 },
             { "node": 54, "message": "You realize that your only chance is to fight back. You jump out of your tent yelling as loudly as you can. The wolves are startled by your sudden outburst and disperse. As you survey your campsite you see that your pack and tent have been ruined. You’re hungry, tired, and out of supplies. Do you want to keep going or sit and try to wait for help.", "help": 55, "walk": 56 },
 			 { "node": 55, "message": "You feel defeated by everything that’s happened to you so far. You find a fallen tree neary and sit down on it. You think that maybe help will arrive if you wait here. As the hours pass by you become weaker and weaker. Soon you are unable to move and you begin to fade in and out of consciousness. Out of the corner of your eyes you see the wolfpack return to finish the job they started earlier today. You have died. Would you like to try again?", "yes": 50, "no": 123 },
		     { "node": 56, "message": "Struggling to your feet you decide to make one last push forward. You can faintly hear the sound of running water and you decide to head towards it. As you make it through the brush a sense of relief floods over you as you see a small creek is flowing. The water looks clear but there is an odd stench permeating the air. Your options are to drink the water or advance.", "drinkthewater": 57, "advance": 58 },
			 { "node": 57, "message": "As you bend down to take a drink you see something under the surface of the water. Looking closer you realize that it is Todd, your party member. His body must have been swept away in the avalanche. You feel sick and begin vomiting. You feel sad at the sight of Todd’s body submerged under the water. Do you try to bury Todd or advance?", "advance": 58, "buryTodd": 59 },
             { "node": 58, "message": "You begin to follow the river down the slope and, once the smell has faded, take a deep drink to replenish yourself. As you walk along the slope the river becomes wider and the current seems faster. You feel a sense of hope at the sight of the surging waters and think that your journey might soon be over. Despite being on your last legs you press onwards and are greeted with the sounds of heavy machinery. Feeling renewed you sprint towards the source of the sounds and find a group of lumberjacks. You have survived Denali. Congratulations!" },
             { "node": 59, "message": "You feel the need to give todd a proper burial. You begin to drag todd out of the water, but the current is stronger than you expected and you feel him beginning to be swept away by the river. Do you keep trying to save todd or do you give up and move on?", "saveTodd": 60, "giveup": 58 },
             { "node": 60, "message": "In spite of the current you still can’t leave todd behind. You pull with all your might but the current is too strong. A surge of water washes todd away, causing you to lose your balance and fall in. YOu hit your head on a rock as you fall and lose consciousness. You have died. Do you want to try again?", "yes": 56, "no": 123 },
             { "node": 123, "message": ""},
];

// These are messages that Alexa says to the user during conversation

// This is the intial welcome message
var welcomeMessage = "Welcome to Denali Descent, are you ready to play?";

// This is the message that is repeated if the response to the initial welcome message is not heard
var repeatWelcomeMessage = "Say yes to start the game or no to quit.";

// this is the message that is repeated if Alexa does not hear/understand the reponse to the welcome message
var promptToStartMessage = "Say yes to continue, or no to end the game.";

// This is the prompt during the game when Alexa doesnt hear or understand a yes / no reply
var promptToSayYesNo = "Say yes or no to answer the question.";

// this is the help message during the setup at the beginning of the game
var helpMessage = "I will ask you some questions that will identify what you would be best at. Want to start now?";

// This is the goodbye message when the user has asked to quit the game
var goodbyeMessage = "Ok, see you next time!";

var speechNotFoundMessage = "Could not find speech for node";

var nodeNotFoundMessage = "In nodes array could not find node";

var loopsDetectedMessage = "A repeated path was detected on the node tree, please fix before continuing";

var utteranceTellMeMore = "tell me more";

var utterancePlayAgain = "play again";

// the first node that we will use
var START_NODE = 1;

// --------------- Handlers -----------------------

// Called when the session starts.
exports.handler = function (event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.registerHandlers(newSessionHandler, startGameHandlers, askQuestionHandlers);
    alexa.execute();
};

// set state to start up and  welcome the user
var newSessionHandler = {
  'LaunchRequest': function () {
    this.handler.state = states.STARTMODE;
    this.emit(':ask', welcomeMessage, repeatWelcomeMessage);
  },'AMAZON.HelpIntent': function () {
    this.handler.state = states.STARTMODE;
    this.emit(':ask', helpMessage, helpMessage);
  },
  'Unhandled': function () {
    this.handler.state = states.STARTMODE;
    this.emit(':ask', promptToStartMessage, promptToStartMessage);
  }
};

// --------------- Functions that control the skill's behavior -----------------------

// Called at the start of the game, picks and asks first question for the user
var startGameHandlers = Alexa.CreateStateHandler(states.STARTMODE, {
    'AMAZON.YesIntent': function () {

        // ---------------------------------------------------------------
        // check to see if there are any loops in the node tree - this section can be removed in production code
        visited = [nodes.length];

		// set state to asking questions
        this.handler.state = states.STORYMODE;

        // ask first question, the response will be handled in the askQuestionHandler
        var message = helper.getSpeechForNode(START_NODE);

        // record the node we are on
        this.attributes.currentNode = START_NODE;

        // ask the first question
        this.emit(':ask', message, message);
    },
    'AMAZON.NoIntent': function () {
        // Handle No intent.
        this.emit(':tell', goodbyeMessage);
    },
    'AMAZON.StopIntent': function () {
        this.emit(':tell', goodbyeMessage);
    },
    'AMAZON.CancelIntent': function () {
        this.emit(':tell', goodbyeMessage);
    },
    'AMAZON.StartOverIntent': function () {
         this.emit(':ask', promptToStartMessage, promptToStartMessage);
    },
    'AMAZON.HelpIntent': function () {
        this.emit(':ask', helpMessage, helpMessage);
    },
    'Unhandled': function () {
        this.emit(':ask', promptToStartMessage, promptToStartMessage);
    }
});

// user will have been asked a question when this intent is called. We want to look at their 
// response and then ask another question.
var askQuestionHandlers = Alexa.CreateStateHandler(states.STORYMODE, {

    'AMAZON.YesIntent': function () {
        // Handle Yes intent.
        helper.yesOrNo(this,'yes');
    },
    'AMAZON.NoIntent': function () {
        // Handle No intent.
         helper.yesOrNo(this, 'no');
    },
	'directionIntent': function () {
        // Handle direction inten
        helper.dirIntent(this, this.event.request.intent.slots.Direction.value);
    },
	'placeIntent': function() {
		// Handle cave intent
		helper.plIntent(this, this.event.request.intent.slots.Place.value);
	},	
	'actionIntent': function () {
		// Handle action intent
		helper.actIntent(this, this.event.request.intent.slots.Action.value);
	},
    'berryIntent': function () {
        // Handle berry intent
        helper.berryIntent(this, this.event.request.intent.slots.Berry.value);
    },
	'AMAZON.HelpIntent': function () {
        this.emit(':ask', promptToSayYesNo, promptToSayYesNo);
    },
    'AMAZON.StopIntent': function () {
        this.emit(':tell', goodbyeMessage);
    },
    'AMAZON.CancelIntent': function () {
        this.emit(':tell', goodbyeMessage);
    },
    'AMAZON.StartOverIntent': function () {
        // reset the game state to start mode
        this.handler.state = states.STARTMODE;
        this.emit(':ask', welcomeMessage, repeatWelcomeMessage);
    },
    'Unhandled': function () {
        this.emit(':ask', promptToSayYesNo, promptToSayYesNo);
    }
});

// --------------- Helper Functions  -----------------------

var helper = {

    // logic to provide the responses to the yes or no responses to the main questions 
    yesOrNo: function (context, reply) {
        // this is a question node so we need to see if the user picked yes or no
        var nextNodeId = helper.getNextNodeYN(context.attributes.currentNode, reply);

        // error in node data
        if (nextNodeId == -1)
        {
            context.handler.state = states.STARTMODE;

            // the current node was not found in the nodes array
            // this is due to the current node in the nodes array having a node id for a node that does not exist
            context.emit(':tell', nodeNotFoundMessage, nodeNotFoundMessage);
        }

        // get the speech for the child node
        var message = helper.getSpeechForNode(nextNodeId);

        // set the current node to next node we want to go to
        context.attributes.currentNode = nextNodeId;

        context.emit(':ask', message, message);
    },

	// logic to provide the response to a directional response to the main questions
	dirIntent: function (context, reply) {
        // this is a question node so we need to see if the user picked east or west
        var nextNodeId = helper.getNextNodeDR(context.attributes.currentNode, reply);

        if (nextNodeId == -1)
        {
            context.handler.state = states.STARTMODE;

            context.emit(':tell', nodeNotFoundMessage, nodeNotFoundMessage);
        }
        var message = helper.getSpeechForNode(nextNodeId);
 
        context.attributes.currentNode = nextNodeId;

        context.emit(':ask', message, message);
    },

	plIntent: function (context, reply) {
        // this is a question node so we need to see if the user picked cave or tent
        var nextNodeId = helper.getNextNodePL(context.attributes.currentNode, reply);

        if (nextNodeId == -1)
        {
            context.handler.state = states.STARTMODE;

            context.emit(':tell', nodeNotFoundMessage, nodeNotFoundMessage);
        }
        var message = helper.getSpeechForNode(nextNodeId);

        context.attributes.currentNode = nextNodeId;

        context.emit(':ask', message, message);
    },
	actIntent: function (context, reply) {
        // this is a question node so we need to see if the user picked explore or store
        var nextNodeId = helper.getNextNodeACT(context.attributes.currentNode, reply);

        if (nextNodeId == -1)
        {
            context.handler.state = states.STARTMODE;

            context.emit(':tell', nodeNotFoundMessage, nodeNotFoundMessage);
        }
        var message = helper.getSpeechForNode(nextNodeId);

        context.attributes.currentNode = nextNodeId;

        context.emit(':ask', message, message);
    },
    berryIntent: function (context, reply) {
        // this is a question node so we need to see if the user picked smooth or wrinkled
        var nextNodeId = helper.getNextNodeBerry(context.attributes.currentNode, reply);

        if (nextNodeId == -1)
        {
            context.handler.state = states.STARTMODE;

            context.emit(':tell', nodeNotFoundMessage, nodeNotFoundMessage);
        }
        var message = helper.getSpeechForNode(nextNodeId);

        context.attributes.currentNode = nextNodeId;

        context.emit(':ask', message, message);
    },

    // returns the speech for the provided node id
    getSpeechForNode: function (nodeId) {

        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].node == nodeId) {
                return nodes[i].message;
            }
        }
        return speechNotFoundMessage + nodeId;
    },

    // gets the next node to traverse to based on the response
    getNextNodeYN: function (nodeId, response) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].node == nodeId) {
                if (response == "yes") {
                    return nodes[i].yes;
				}
				return nodes[i].no;
            }
        }
		// error condition, didnt find a matching node id. Cause will be an entry in the array but with no corrosponding array entry
        return -1;
    },
	getNextNodeDR: function (nodeId, response) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].node == nodeId) {
				if (response == "east") {
					return nodes[i].east;
				}
				if (response == "west") {
                    return nodes[i].west;
                }
                if (response == "side") {
                    return nodes[i].side;
                }
                if (response == "down") {
                    return nodes[i].down;
                }
                if (response == "rock") {
                    return nodes[i].rock;
				}
            }
        }
        return -1;
    },
  getNextNodePL: function (nodeId, response) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].node == nodeId) {
				if (response == "cave") {
					return nodes[i].cave;
				}
				else if (response == "tent") {
                    return nodes[i].tent;
				}
                else if (response == "advance") {
                    return nodes[i].advance;
                }
                else if (response == "alcove") {
                    return nodes[i].alcove;
                }
                else if (response == "bunker") {
                    return nodes[i].bunker;
                }
            }
        }
        return -1;
    },
  getNextNodeACT: function (nodeId, response) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].node == nodeId) {
				if (response == "explore") {
					return nodes[i].explore;
				}
                else if (response == "store") {
				    return nodes[i].store;
                }
				else if (response == "follow") {
                    return nodes[i].follow;
                }
                else if (response == "sleep") {
                    return nodes[i].sleep;
                }
                else if (response == "camp") {
                    return nodes[i].camp;
                }
                else if (response == "go") {
                    return nodes[i].go;
                }
                else if (response == "jump") {
                    return nodes[i].jump;
                }
                else if (response == "walk") {
                    return nodes[i].walk;
                }
                else if (response == "pet") {
                    return nodes[i].pet;
                }
                else if (response == "wait") {
                    return nodes[i].wait;
                }
                else if (response == "trace") {
                    return nodes[i].trace;
                }
                else if (response == "other") {
                    return nodes[i].jump;
                }
                else if (response == "climb") {
                    return nodes[i].climb;
                }
                else if (response == "run") {
                    return nodes[i].run;
                }
                else if (response == "play dead") {
                    return nodes[i].playdead;
                }
                else if (response == "fight the wolf") {
                    return nodes[i].fightthewolf;
                }
                else if (response == "help") {
                    return nodes[i].help;
                }

            }
        }
        return -1;
  },
    getNextNodeBerry: function (nodeId, response) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].node == nodeId) {
                if (response == "smooth") {
                    return nodes[i].smooth;
                }
                return nodes[i].wrinkled;
            }
        }
        return -1;
    },
  };
