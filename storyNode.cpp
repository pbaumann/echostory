// Philip Baumann
// Data Structures Final Echo Project
// 19 March 2017
// storyNode.cpp - c++ implementation of an objection potentially used in our project 

#include <iostream>
#include "storyNode.h"
#include <vector>
using namespace std; 

storyNode::storyNode() {
  dial = "none";
  character = "defaultChar";
  pauseStory = false;
  storyNode *next;
  storyNode nextPath("nextDefault", "defaultChar");
  next = &nextPath;
  addPath(next);
}

storyNode::storyNode(string dialInput, string characterInput) {
  dial = dialInput;
  character = characterInput;
}

storyNode::~storyNode() {} // may need to use dynamic memory allocation

void storyNode::setDial( string newDial) {
  dial = newDial;
}

void storyNode::setCharacter( string newChar) {
  character = newChar;
}

string storyNode::getDial() {
  return dial;
}

void storyNode::printDial() { // returns the dialogue of the story node
  cout << character << ": " << dial << endl;
}

void storyNode::addPath(storyNode* newSN) {
  paths.push_back(newSN);
}

int storyNode::getPath(string input) {
  int length = paths.size();
  for (int i = 0; i < length; i++ ) {
    if (input == paths[i+1]->getDial()) return i+1;
    else return -1; // -1 signifies invalid input
  }
}

storyNode* storyNode::nextPath(int pathNumber) {
  return paths[pathNumber];
}
