// Philip Baumann
// Data Structures Final Echo Project
// 6 March 2017
// storyNode.h - c++ implementation of an object potentially used in our program

#ifndef STORYNODE_H
#define STORYNODE_H
#include <iostream>
#include <vector>

using namespace std;

class storyNode {
  public:
    storyNode(); // A non-default constructor must also be made. Any ideas?
    storyNode(string,string);
    ~storyNode();
    void setDial(string);
    void setCharacter(string);
    void printDial(); // Prints the dialogue of the node
    string getDial();
    void addPath( storyNode *); 
    int getPath(string); // The string is the user input, path is chosen off this
    storyNode * nextPath(int); // Gets the next story node from the vector of story Nodes.
  private:
    vector <storyNode*> paths;
    string dial;
    string character; //user can play as 3 different characters
    bool pauseStory; //makes the player wait a certain time if true
};
#endif
