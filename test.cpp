// Philip Baumann
// Data Structures - Final Echo Project
// 19 March 2017
// test.cpp - testing out the storyNode c++ demo implementation

#include <iostream>
#include <vector>
#include "storyNode.h"

using namespace std;

int main() {
  storyNode part1;
  storyNode part2;
  part1.setDial("Hello, can you hear me?");
  part1.setCharacter("Alexa");
  part1.printDial();
  part2.setDial("I can hear you.");
  part2.setCharacter("User");
  part2.printDial();
  storyNode *daughter;
  daughter = &part2;
  part1.addPath(daughter);
  int nodeNumber = part1.getPath("I can hear you.");
  cout << endl << "Node being printed as daughter of part1: " << endl;
  part1.nextPath(nodeNumber)->printDial();
}
