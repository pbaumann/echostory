// Michael Wang
// Data Structures - Final Echo Project
// 21 March 2017
// Taking text from a text file and being able to manipulate it so that it can be accessed efficiently

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include "storyNode.h"

using namespace std;

//determines whether the text contains an integer first which represents a different story path
bool is_digits(string str) {
	 return str.find_first_not_of("0123456789") == std::string::npos;
}

int main(int argc, char *argv[])
{
	string dial;
	int userDial[10] = {0};
	int optionDial[10] = {0};
	int commandDial[10] = {0};
	int bearDial[10] = {0}; //may want to use dynamic memory, so far 10 is the amount of story nodes
	int iterator = 0;

	ifstream storyfile("storyline.txt");
	if (storyfile.is_open())
	{
		while (getline(storyfile, dial))
		{
			if (is_digits(dial)) {
				cout << "Here are the options for dialogue " << dial << endl;
				cout << endl;
				iterator++; 
			}

			if (dial.find("Bear:") != string::npos)
				bearDial[iterator] = bearDial[iterator] + 1;
			else if (dial.find("Option:") != string::npos)
				optionDial[iterator] = optionDial[iterator] + 1;
			else if (dial.find("Command:") != string::npos)
				commandDial[iterator] = commandDial[iterator] + 1;
			else if (dial.find("User:") != string::npos)
				userDial[iterator] = userDial[iterator] + 1;

			//Philip's code that can be used with mine to hopefully create a more efficient way of combine story with a tree
			// cout << dial << '\n';
			// storyNode part1;
			// storyNode part2;
			// part1.setDial("Hello, can you hear me?");
			// part1.setCharacter("Alexa");
			// part1.printDial();
			// part2.setDial("I can hear you.");
			// part2.setCharacter("User");
			// part2.printDial();
			// storyNode *daughter;
			// daughter = &part2;
			// part1.addPath(daughter);
			// int nodeNumber = part1.getPath("I can hear you.");
			// cout << endl << "Node being printed as daughter of part1: " << endl;
			// part1.nextPath(nodeNumber)->printDial();
		}
		storyfile.close();

		for (int i = 1; i < 10; i++)
			cout << "Within story node " << i << " there are " << bearDial[i] << " dialogues for the bear, " << userDial[i] << " dialogues for the user, " << optionDial[i] << " different options, and " << commandDial[i] << " different commands that can be given to the computer." << endl;
	}
	else cout << "Can't open file";

	return 0;
}
