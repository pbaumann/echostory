public class storyNode{
  String dial = "none";
  String character = "defaultChar";
  boolean pauseStory = false;
 

  public void setDial(String newDial){
    dial = newDial;
  }

  public void setCharacter(String newChar){
    character = newChar;
  }

  public String getDial(){
    return dial;
  }

  public void printDial(){
    System.out.println(character + ": " + dial);
  }
  

  public static void main(String[] args){
    storyNode test = new storyNode();
    test.printDial();
  } 
}
